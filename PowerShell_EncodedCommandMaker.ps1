[string]$psfilename = read-host "Enter the Powershell filename for encoding"
$psfilename = $psfilename -replace '"',''
$psfilecontect = gc "$psfilename"
$command = $psfilecontect | out-string
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
$encodedCommand = [Convert]::ToBase64String($bytes)
"@echo off" | Out-File -Force -Encoding ASCII -FilePath ("$psfilename"+".bat")
"powershell.exe -encodedCommand "+$encodedCommand | Out-File -Append -Encoding ASCII -FilePath ("$psfilename"+".bat")